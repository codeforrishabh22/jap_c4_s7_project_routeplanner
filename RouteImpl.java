package com.rishabh.routePlanner;

public class RouteImpl {
    public static void main(String[] args) {
        RoutePlanner routePlanner = new RoutePlanner();

        String fileNameWithAddress = "C:\\Users\\ayush\\IdeaProjects\\JAP_C4_S7_Project_RoutePlanner\\src\\com\\rishabh\\routePlanner\\csvfile\\input.txt";
        int count = routePlanner.countOfRoutes(fileNameWithAddress);
        Routes[] routes = routePlanner.readRoutes(fileNameWithAddress,count);
        System.out.println("Task one read data from csv and print\n\n");
        System.out.println("From          To    Distance    Travel Time    Airfare");
        for(Routes r:routes){

            System.out.println(r.toString());
        }
        System.out.println("Task two showDirect flights from a city\n\n");
        String fromCity="Delhi";
        routePlanner.showDirectFlights(routes,fromCity);
        System.out.println("Task three sort flights\n\n");

        routePlanner.sortDirectFlights(routes);

        System.out.println("Task four find fights ");

        routePlanner.showFlights(routes,"Delhi","London");
    }
}
