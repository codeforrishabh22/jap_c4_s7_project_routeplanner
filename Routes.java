package com.rishabh.routePlanner;

public class Routes implements Comparable<Routes> {
    private String source;
    private String destination;
    private Integer distance;
    private String travelTime;
    private String airfare;

    public Routes() {
    }

    public Routes(String source, String destination, Integer distance, String travelTime, String airfare) {
        this.source = source;
        this.destination = destination;
        this.distance = distance;
        this.travelTime = travelTime;
        this.airfare = airfare;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getTravelTime() {
        return travelTime;
    }

    public void setTravelTime(String travelTime) {
        this.travelTime = travelTime;
    }

    public String getAirfare() {
        return airfare;
    }

    public void setAirfare(String airfare) {
        this.airfare = airfare;
    }

    @Override
    public String toString() {
        return source+"       "+destination+"    "+distance+"       "+travelTime+"        "+airfare;
    }


    @Override
    public int compareTo(Routes o) {
        return this.destination.compareTo(o.destination);
    }
}
