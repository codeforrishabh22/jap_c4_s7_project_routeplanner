package com.rishabh.routePlanner;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.*;

public class RoutePlanner {

    public RoutePlanner() {
    }

    public int countOfRoutes(String filename){
        try {
            int recordCounts = -1;
            BufferedReader fileread = new BufferedReader(new FileReader(filename));
            String line=null;
            while ((line = fileread.readLine())!=null){
               recordCounts++;
            }
            fileread.close();
            return recordCounts;
        }catch (Exception e){
            return -1;
        }
    }
    public Routes[] readRoutes(String filename,int count){
        Routes[] routes = new Routes[count];
        for(int i=0;i<count;i++){
            routes[i] = new Routes();
        }
        try{
            BufferedReader fileread = new BufferedReader(new FileReader(filename));
            String line = null;
            String[] t_routes;
            int chk=0;
            int i=0;
            while((line = fileread.readLine())!=null){
                t_routes = line.split(",");
                if(chk>0){
                    routes[i].setSource(t_routes[0]);
                    routes[i].setDestination(t_routes[1]);
                    routes[i].setDistance(Integer.parseInt(t_routes[2]));
                    routes[i].setTravelTime(t_routes[3]);
                    routes[i].setAirfare(t_routes[4]);
                    i++;
                }
                chk++;
            }
            return routes;
        }catch (Exception e){
            return routes;
        }

    }

    public void showDirectFlights(Routes[] routes,String fromCity){
        int i=0;
        for(Routes r:routes){
            if(r.getSource().equals(fromCity)){
                if(i==0)System.out.println("From          To    Distance    Travel Time    Airfare");
                System.out.println(r);
                i++;
            }
        }
        if(i==0)System.out.println("We are sorry. At this point of time, we do not have any information on flights originating from "+fromCity+".");
    }
    public void sortDirectFlights(Routes[] routes){
        ArrayList<Routes> list = new ArrayList<Routes>(Arrays.asList(routes));
        Collections.sort(list);
        for(Routes r: list){
            System.out.println(r);
        }
    }

    public void showFlights(Routes[] routes, String fromCity, String toCity){
        for (Routes route : routes) {
            if (route.getSource().equals(fromCity) && route.getDestination().equals(toCity)) {
                System.out.println(route);
            }
        }
        System.out.println();
        for(int i=0;i<routes.length;i++){
            for(int j=0;j<routes.length;j++){
                if(i!=j) {
                    if(routes[i].getSource().equals(fromCity)&&routes[i].getDestination().equals(routes[j].getSource()) && routes[j].getDestination().equals(toCity)){
                        System.out.println(routes[i].toString());
                        System.out.println(routes[j].toString());
                    }
                }

            }
        }
    }
}
